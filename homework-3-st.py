
# coding: utf-8

# # Домашнее задание 3. Алгоритмы кластеризации

# Основная задача этого задания -- реализация одного из алгоритмов кластеризации. Кроме того, мы научимся подбирать параметры алгоритма, визуализируем результат кластеризации и попытаемся проанализировать природу полученных кластеров.

# In[1]:

import pandas as pd
import pylab as pl
import numpy as np
import scipy.spatial as ss
import sklearn.cluster as sc
import sklearn.manifold as sm
import sklearn.datasets as ds
import sklearn.metrics as smt

# Plotting config
get_ipython().magic(u'pylab inline')


# Cчитываем данные, полученные в результате выполнения предыдущего домашнего задания, в `data frame` библиотеки `pandas`. Конвертируем данные в массив `numpy`.

# In[3]:

data_df = pd.read_csv("hw2_out.csv", sep="\t", header=0, index_col="uid")
x = data_df.values


# Алгоритм кластеризации, который необходимо реализовать, выбирается на основании Вашего логина на портале Техносферы. При проверке домашнего задания бадут осуществляться проверка логина и соответствующего алгоритма.

# In[3]:

algorithms = [
    u"Gaussian Mixture Model с использованием maximum a-posteriori для выбора кластера (параметр - k)",
    u"Hierarchical clustering с поддержкой single-linkage, complete-linkage, average-linkage (параметры - k, linkage)",
    u"DBSCAN. (параметры - epsilon, min_pts)", 
    u"OPTICS. (параметры - epsilon, min_pts)"
]

my_algorithm = algorithms[hash("e.pavlova") % len(algorithms)]
print u"Реализуем алгоритм кластеризации '%s'" % my_algorithm


# Алгоритм кластеризации должен удовлетворять следующему интерфейсу. Конструктор принимает набор параметров, необходимых для работы алгоритма кластеризации. Метод `fit` подсчитывает параметры модели и возвращает `self`. Метод `predict` возвращает вектор с индексами кластеров для поданных в него объектов `x`

# In[2]:

UNDEFINED = None
class Clustering:
    """
    Implement clustering algorithm according 
    """
    
    def __init__(self, eps = 1, min_pts = 2):
        self.eps = eps
        self.min_pts = min_pts
        return
        
    def fit(self, x, y=None):
        """
        Use data matrix x to compute model parameters
        """
        x_new = []
        for obj in x:
            obj = {"features" : obj, "reachability_dist" : UNDEFINED, "processed" : False, "core_distance" : UNDEFINED}
            x_new.append(obj)
        self.unproc = [i for i in x_new]
        self.points = x_new
        self.ordered = []
        return self
    
    def get_neighbors(self, obj):
        neighbors = []
        for o in self.points:
            if np.linalg.norm(o["features"] - obj["features"]) <= self.eps :
                if o["features"] is not obj["features"] :
                    neighbors.append(o)
        return neighbors
    
    def mark_processed(self, obj):
        idx = 0
        for elem in self.unproc :
            if len(set(elem["features"]) - set(obj["features"])) == 0:
                del self.unproc[idx]
                #self.unproc.remove(elem)
            idx += 1
        obj["processed"] = True
    
    def core_distance(self, obj, N) : 
        if obj["core_distance"] is not None :
            return obj["core_distance"]
        if len(N) > self.min_pts :
            sort_N = [np.linalg.norm(p["features"] - obj["features"]) for p in N]
            sort_N.sort()
            obj["core_distance"] = sort_N[self.min_pts - 2]
            return obj["core_distance"]
    
    def update(self, N, obj, Seeds) :
        obj["core_distance"] = self.core_distance(obj, N)
        for z in N :
            if z["processed"] == False :
                new_reach_dist = np.maximum(obj["core_distance"], np.linalg.norm(obj["features"] - z["features"]))
                if z["reachability_dist"] is UNDEFINED :
                    z["reachability_dist"] = new_reach_dist
                    Seeds.append(z)
                else :
                    if new_reach_dist < z["reachability_dist"] :
                        z["reachability_dist"] = new_reach_dist
        return 
    
    def predict(self, x):
        """
        Using computed model parameters predict cluster
        for all objects from x
        """
        while self.unproc:
            print "ok"
            obj = self.unproc[0]
            N = self.get_neighbors(obj)
            self.mark_processed(obj)
            self.ordered.append(obj)
            if self.core_distance(obj, N) is not UNDEFINED :
                Seeds = []
                self.update(N, obj, Seeds)
                for z in Seeds:
                    #Seeds.sort(key = lambda n: n["reachability_dist"])
                    #z = Seeds[0]
                    N_ = self.get_neighbors(z)
                    self.mark_processed(z)
                    self.ordered.append(z)
                    if self.core_distance(z, N_) is not UNDEFINED :
                        self.update(N_, z, Seeds)
        return self.ordered
    
    
    def fit_predict(self, x, y=None):
        self.fit(x, y)
        return self.predict(x)


# Сначала проверим реализованный алгоритм кластеризации на классическом наборе данных [Iris](http://www.wikiwand.com/en/Iris_flower_data_set). Загрузим данные (они включены в библиотеку sklearn) и посмотрим на то, как они выглядят в двух проекциях (для простоты используем 2 класса из 3).

# In[3]:

iris = ds.load_iris()
x_iris = iris.data[:100]
print dir(x_iris)
y_iris = iris.target[:100]

pl.figure(figsize=(10, 5))

pl.subplot(1, 2, 1)
pl.scatter(x_iris[:, 0], x_iris[:, 1], c=y_iris, cmap=pl.cm.PuOr, lw=0, s=30)
plt.xlabel('Sepal length')
plt.ylabel('Sepal width')

pl.subplot(1, 2, 2)
pl.scatter(x_iris[:, 2], x_iris[:, 3], c=y_iris, cmap=pl.cm.PuOr, lw=0, s=30)
plt.xlabel('Petal length')
plt.ylabel('Petal width')
pl.show()


# Видно, что классы разделяются, поэтому можно надеяться, что наш алгоритм "найдет" кластеры, соответствующие исходным классам. Результат работы реализованного алгоритма кластеризации предлагается сравнить с эталонной кластеризацией. Для этого предлагается изучить метрику ([adjusted rand score](http://scikit-learn.org/stable/modules/generated/sklearn.metrics.adjusted_rand_score.html)). В случае если значение этой метрики отличается от 1, предлагается поработать над улучшением реализации своего алгоритма.

# In[ ]:

pred_iris = Clustering().fit_predict(x_iris)
print "Adjusted Rand index for iris is: %.2f" % smt.adjusted_rand_score(y_iris, pred_iris)


# Выбрать и реализовать критерий качества, на основании которого будут подбираться параметры модели. Варианты критериев:
# - Инерция (сумма квадратов расстояний от каждой из точек до ближайшего к этой точке центроида)
# - Средний диаметр (максимальное расстояние между точками в кластере) или радиус (расстояние от центроида до самой дальней от него точки в кластере)
# - Sihouette
# 
# **Критерий качества необходимо выбрать таким образом, чтобы он подходил для реализованного алгоритма**

# In[ ]:

def quality(x, y):
    """
    Implement quality criterion of your choice
    """
    return 0.51


# Применим критерий для выбора параметров алгоритма. Предлагается изменить следующий код таким образом, чтобы кластеризация вызывалась с верным набором параметров. На графике можно будет увидеть зависимость критерия от параметров алгоритма. Необходимо выбрать оптимальные значения этих параметров.

# In[ ]:

ks = range(1, 21)
criteria = np.zeros(len(ks))

for i, k in enumerate(ks):
    cls = Clustering()
    y = cls.fit_predict(x)
    criteria[i] = quality(x, y)
    
pl.figure(figsize=(8, 6))
pl.plot(ks, criteria)
pl.title("$J(k)$")
pl.ylabel("Criteria $J$")
pl.xlabel("Number of clusters $k$")
pl.grid()
pl.show()


# Далее необходимо кластеризовать данные, используя выбранное количество кластеров, и визуализировать результат с помощью T-SNE. Внимание! На полном объеме данных данный алгоритм может работать существенное время. 

# In[ ]:

k = 5
cls = Clustering()
y = cls.fit_predict(x)


# Визуализируем результаты кластеризации ([подсказка](http://anokhin.github.io/img/hw3_tsne-kmeans.png)). Можно ли сказать, что в двумерном пространстве объекты из одного кластера находятся недалеко? Какой вывод можно сделать о качестве кластеризации?

# In[ ]:

tsne = sm.TSNE(n_components=2, verbose=1, n_iter=1000)
z = tsne.fit_transform(x)

# Color map
cm = pl.get_cmap('jet')
pl.figure(figsize=(15, 15))
pl.scatter(z[:, 0], z[:, 1], c=map(lambda c: cm(1.0 * c / k), y))
pl.axis('off')
pl.show()


# Попробуем проанализировать полученные кластеры. Для того, чтобы интерпретировать "суть" каждого из кластеров, построим [radar plot](http://www.wikiwand.com/en/Radar_chart), отображающий их центроиды. Посмотрите на графики и попробуйте объяснить, каким поведением можно охарактеризовать каждый из полученных кластеров? Есть ли среди кластеров похожие?
# 
# Реализация радара позаимствована отсюда: 
# http://www.science-emergence.com/Matplotlib/MatplotlibGallery/RadarChartMatplotlibRougier/

# In[ ]:

def radar(centroid, features, axes, color):
    # Set ticks to the number of features (in radians)
    t = np.arange(0, 2*np.pi, 2*np.pi/len(features))
    plt.xticks(t, [])

    # Set yticks from 0 to 1
    plt.yticks(np.linspace(0, 1, 6))

    # Draw polygon representing centroid
    points = [(x, y) for x, y in zip(t, centroid)]
    points.append(points[0])
    points = np.array(points)
    codes = [path.Path.MOVETO,] + [path.Path.LINETO,] * (len(centroid) - 1) + [ path.Path.CLOSEPOLY ]
    _path = path.Path(points, codes)
    _patch = patches.PathPatch(_path, fill=True, color=color, linewidth=0, alpha=.3)
    axes.add_patch(_patch)
    _patch = patches.PathPatch(_path, fill=False, linewidth = 2)
    axes.add_patch(_patch)

    # Draw circles at value points
    plt.scatter(points[:,0], points[:,1], linewidth=2, s=50, color='white', edgecolor='black', zorder=10)

    # Set axes limits
    plt.ylim(0, 1)

    # Draw ytick labels to make sure they fit properly
    for i in range(len(features)):
        angle_rad = i/float(len(features))*2*np.pi
        angle_deg = i/float(len(features))*360
        ha = "right"
        if angle_rad < np.pi/2 or angle_rad > 3*np.pi/2: ha = "left"
        plt.text(angle_rad, 1.05, features[i], size=7, horizontalalignment=ha, verticalalignment="center")

# Some additiola imports
import matplotlib
import matplotlib.path as path
import matplotlib.pyplot as plt
import matplotlib.patches as patches

# Choose some nice colors
matplotlib.rc('axes', facecolor = 'white')
# Make figure background the same colors as axes 
fig = plt.figure(figsize=(15, 15), facecolor='white')

cm = pl.get_cmap('jet')

clusters = np.unique(y)
for j, cluster in enumerate(clusters):    
    x_c = x[y == cluster]
    centroid = x_c.mean(axis=0)    
    # Use a polar axes
    axes = plt.subplot(3, 3, j + 1, polar=True)
    radar(centroid, data_df.columns.values, axes, cm(1.0 * j / k))

plt.show()


# Поздравляем, Вы завершили домашние задания по модулю "Алгоритмы кластеризации". Надеемся, что это было интересно.
